# main.tf

###############################################################################
# DO NOT CHANGE - These resources use for_each to loop through the array that #
# is defined in terraform.tfvars.json where you can add or change records.    #
###############################################################################

###############################################################################
# Managed Zone Resource                                                       #
###############################################################################

# Zone for root level domain
# If the zone already exists, see `README.md` for instructions on running the
# `terraform import` command or using the Terraform Import Zone GitLab CI job.
resource "google_dns_managed_zone" "root_zone" {
  description = var.dns_zone_description
  dns_name    = var.dns_zone_fqdn
  name        = var.dns_zone_name
  project     = var.gcp_project

  dnssec_config {
    state = var.dnssec_enabled ? "on" : "off"
  }
}

###############################################################################
# A Records                                                                   #
###############################################################################

# Create @ root DNS A record with the IPv4 address value.
resource "google_dns_record_set" "record_a_root" {
  count = length(var.root_a_record) > 0 ? 1 : 0

  managed_zone = google_dns_managed_zone.root_zone.name
  name         = google_dns_managed_zone.root_zone.dns_name
  project      = var.gcp_project
  rrdatas      = var.root_a_record
  ttl          = "300"
  type         = "A"

  depends_on = [
    google_dns_managed_zone.root_zone,
  ]
}

# Create DNS A records for subdomains and hosts with the IPv4 address value.
resource "google_dns_record_set" "record_a" {
  for_each = var.a_records

  managed_zone = google_dns_managed_zone.root_zone.name
  name         = "${each.key}.${google_dns_managed_zone.root_zone.dns_name}"
  project      = var.gcp_project
  rrdatas      = [each.value]
  ttl          = "300"
  type         = "A"

  depends_on = [
    google_dns_managed_zone.root_zone,
  ]
}

###############################################################################
# CNAME Records                                                               #
###############################################################################

resource "google_dns_record_set" "record_cname" {
  for_each = var.cname_records

  managed_zone = google_dns_managed_zone.root_zone.name
  name         = "${each.key}.${google_dns_managed_zone.root_zone.dns_name}"
  project      = var.gcp_project
  rrdatas      = [each.value]
  ttl          = "300"
  type         = "CNAME"

  depends_on = [
    google_dns_managed_zone.root_zone,
  ]
}

###############################################################################
# MX Records                                                                  #
###############################################################################

# Create @ root DNS MX records
resource "google_dns_record_set" "record_mx_root" {
  count = length(var.root_mx_records) > 0 ? 1 : 0

  managed_zone = google_dns_managed_zone.root_zone.name
  name         = google_dns_managed_zone.root_zone.dns_name
  project      = var.gcp_project
  rrdatas      = var.root_mx_records
  ttl          = "300"
  type         = "MX"

  depends_on = [
    google_dns_managed_zone.root_zone,
  ]
}

# Create subdomain DNS MX records
resource "google_dns_record_set" "record_mx_subdomain" {
  for_each = var.subdomain_mx_records

  managed_zone = google_dns_managed_zone.root_zone.name
  name         = "${each.key}.${google_dns_managed_zone.root_zone.dns_name}"
  project      = var.gcp_project
  rrdatas      = each.value
  ttl          = "300"
  type         = "MX"

  depends_on = [
    google_dns_managed_zone.root_zone,
  ]
}

###############################################################################
# DNS Subdomain Zones                                                         #
# --------------------------------------------------------------------------- #
# Create DNS record with each of the NS records to add to root level domain   #
# for subdomain zone. The TTL is set at 300 for fast redeployments.           #
###############################################################################

resource "google_dns_record_set" "subzone" {
  for_each = var.subzones

  managed_zone = google_dns_managed_zone.root_zone.name
  name         = "${each.key}.${google_dns_managed_zone.root_zone.dns_name}"
  project      = var.gcp_project
  rrdatas      = each.value
  ttl          = "300"
  type         = "NS"

  depends_on = [
    google_dns_managed_zone.root_zone,
  ]
}

###############################################################################
# TXT records                                                                 #
# --------------------------------------------------------------------------- #
# Be sure to use \" escapes for quotes.                                       #
# Example. "\"v=spf1 include:mailgun.org ~all\""                              #
###############################################################################

# Create DNS Records for TXT Record
resource "google_dns_record_set" "record_txt" {
  for_each = var.txt_records

  managed_zone = google_dns_managed_zone.root_zone.name
  name         = "${each.key}.${google_dns_managed_zone.root_zone.dns_name}"
  project      = var.gcp_project
  rrdatas      = [each.value]
  ttl          = "300"
  type         = "TXT"

  depends_on = [
    google_dns_managed_zone.root_zone,
  ]
}
