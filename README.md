# GCP Terraform Module for Cloud DNS Managed Zone and Records

This Terraform module is designed to be used for creating a Cloud DNS Managed Zone and using the variable file for defining all of the DNS record sets.

## Table of Contents

- [GCP Terraform Module for Cloud DNS Managed Zone and Records](#gcp-terraform-module-for-cloud-dns-managed-zone-and-records)
  - [Table of Contents](#table-of-contents)
  - [Getting Started Instructions](#getting-started-instructions)
    - [New Project Example](#new-project-example)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Modules](#modules)
  - [Resources](#resources)
  - [Inputs](#inputs)
  - [Outputs](#outputs)
    - [Accessing Output Values](#accessing-output-values)
  - [Authors and Maintainers](#authors-and-maintainers)

## Getting Started Instructions

See [INSTALL.md](INSTALL.md) for step-by-step instructions for using this Terraform module.

### New Project Example

This example includes a basic configuration to get you started with using this module.
* [examples/new-project/main.tf](examples/new-project/main.tf)
* [examples/new-project/outputs.tf](examples/new-project/outputs.tf)
* [examples/new-project/terraform.tfvars.json](examples/new-project/terraform.tfvars.json)
* [examples/new-project/variables.tf](examples/new-project/variables.tf)

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.4 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.13 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.13 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_dns_managed_zone.root_zone](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_managed_zone) | resource |
| [google_dns_record_set.record_a](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |
| [google_dns_record_set.record_a_root](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |
| [google_dns_record_set.record_cname](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |
| [google_dns_record_set.record_mx_root](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |
| [google_dns_record_set.record_mx_subdomain](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |
| [google_dns_record_set.record_txt](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |
| [google_dns_record_set.subzone](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |

## Inputs

You can see example usage in [examples/new-project/terraform.tfvars.json](examples/new-project/terraform.tfvars.json).

<table>
<thead>
<tr>
    <th style="width: 25%;">Variable Key</th>
    <th style="width: 40%;">Description</th>
    <th style="width: 10%;">Required</th>
    <th style="width: 25%;">Example Value</th>
</tr>
</thead>
<tbody>
<tr>
    <td>
        <code>dns_zone_description</code>
    </td>
    <td>A short description of the purpose of this zone. You can also provide the URL to the GitLab Project that has the Terraform source code and CI pipelines.</a></td>
    <td>No</td>
    <td><code>Created with the gcp-cloud-dns-managed-zone-tf-module</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>dns_zone_fqdn</code>
    </td>
    <td>The FQDN of the DNS zone with a trailing period</a></td>
    <td><strong>Yes</strong></td>
    <td><code>domain.tld.</code></td>
</tr>
<tr>
    <td>
        <code>dns_zone_name</code>
    </td>
    <td>The slug name of the DNS zone</a></td>
    <td><strong>Yes</strong></td>
    <td><code>domain-tld-zone</code></td>
</tr>
<tr>
    <td>
        <code>dnssec_enabled</code>
    </td>
    <td>Enable <a target="_blank" href="https://cloud.google.com/dns/docs/dnssec">DNSSEC</a> for this managed zone with default secure configuration.</a></td>
    <td>No</td>
    <td><code>true</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>gcp_project</code>
    </td>
    <td>The <a target="_blank" href="https://cloud.google.com/resource-manager/docs/creating-managing-projects">GCP project ID</a> (may be an alphanumeric slug) that the resources are deployed in.</a></td>
    <td><strong>Yes</strong></td>
    <td><code>my-project-name</code></td>
</tr>
<tr>
    <td>
        <code>root_a_record</code>
    </td>
    <td>A map with the root A record configuration.</a></td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>    "enabled": false,</code><br />
        <code>    "records": "1.2.3.4"</code><br />
        <code>}</code><br />
    </td>
</tr>
<tr>
    <td>
        <code>a_records</code>
    </td>
    <td>A map with record name and IP address value.</a></td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>    "host1": "1.2.3.4",</code><br />
        <code>    "host2": "5.6.7.8"</code><br />
        <code>}</code>
    </td>
</tr>
<tr>
    <td>
        <code>cname_records</code>
    </td>
    <td>A map with record name and CNAME value.</a></td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>    "host3": "app1.myservice.example.com.",</code><br />
        <code>    "host4": "api.myservice.example.com."</code><br />
        <code>}</code>
    </td>
</tr>
<tr>
    <td>
        <code>root_mx_records</code>
    </td>
    <td>A list with MX values for top-level domain.</a></td>
    <td>No</td>
    <td>
        <code>[</code><br />
        <code>    "1 aspmx.l.google.com.",</code><br />
        <code>    "5 alt1.aspmx.l.google.com.",</code><br />
        <code>    "5 alt2.aspmx.l.google.com.",</code><br />
        <code>    "10 alt3.aspmx.l.google.com.",</code><br />
        <code>    "10 alt4.aspmx.l.google.com."</code><br />
        <code>]</code>
    </td>
</tr>
<tr>
    <td>
        <code>subdomain_mx_records</code>
    </td>
    <td>A map with MX record name and MX value list for subdomains</a></td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>    "mg": [</code><br />
        <code>        "10 mxa.mailgun.org.",</code><br />
        <code>        "10 mxb.mailgun.org."</code><br />
        <code>    ]</code><br />
        <code>}</code><br />
    </td>
</tr>
<tr>
    <td>
        <code>subzones</code>
    </td>
    <td>A map with the subdomain name and a list of name servers that host the subzone configuration.</a></td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>    "myservice": [</code><br />
        <code>        "ns-cloud-a1.googledomains.com.",</code><br />
        <code>        "ns-cloud-a2.googledomains.com.",</code><br />
        <code>        "ns-cloud-a3.googledomains.com.",</code><br />
        <code>        "ns-cloud-a4.googledomains.com."</code><br />
        <code>    ],</code><br />
        <code>    "anotherservice": [</code><br />
        <code>        "ns-1032.awsdns-01.org.",</code><br />
        <code>        "ns-491.awsdns-61.com.",</code><br />
        <code>        "ns-867.awsdns-44.net.",</code><br />
        <code>        "ns-1805.awsdns-33.co.uk."</code><br />
        <code>    ]</code><br />
        <code>}</code>
    </td>
</tr>
<tr>
    <td>
        <code>txt_records</code>
    </td>
    <td>A map with TXT record name and TXT value</a></td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>"mg": "\"v=spf1\" \"include:mailgun.org\" \"~all\""</code><br />
        <code>}</code>
    </td>
</tr>
</tbody>
</table>

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_record_a_root"></a> [record\_a\_root](#output\_record\_a\_root) | A map with the root A record configuration. |
| <a name="output_record_mx_root"></a> [record\_mx\_root](#output\_record\_mx\_root) | A list with MX values for top-level domain. |
| <a name="output_record_mx_subdomain"></a> [record\_mx\_subdomain](#output\_record\_mx\_subdomain) | A map with MX record name and MX value list for subdomains |
| <a name="output_records_a"></a> [records\_a](#output\_records\_a) | A map with record name and IP address value. |
| <a name="output_records_cname"></a> [records\_cname](#output\_records\_cname) | A map with record name and CNAME value. |
| <a name="output_records_txt"></a> [records\_txt](#output\_records\_txt) | A map with TXT record name and TXT value |
| <a name="output_root_zone"></a> [root\_zone](#output\_root\_zone) | Cloud DNS Managed Zone configuration |
| <a name="output_subzone"></a> [subzone](#output\_subzone) | A map with the subdomain name and a list of name servers that host the subzone configuration. |

### Accessing Output Values

The outputs are returned as a map with sub arrays that use dot notation. You can see how each output is defined in [outputs.tf](outputs.tf).

```hcl
# Get a map with all values for the module
module.{{name}}

# Get map of each record type
module.{{name}}.record_a_root
module.{{name}}.records_a
module.{{name}}.records_cname
module.{{name}}.record_mx_root
module.{{name}}.record_mx_subdomain
module.{{name}}.records_txt
module.{{name}}.subzone

# Get individual values
module.{{name}}.root_zone
module.{{name}}.record_a_root.enabled
module.{{name}}.record_a_root.name
module.{{name}}.record_a_root.id
module.{{name}}.record_a_root.records
module.{{name}}.record_a_root.ttl
module.{{name}}.record_a_root.type
module.{{name}}.records_a.{{key}}
module.{{name}}.records_a.{{key}}.name
module.{{name}}.records_a.{{key}}.id
module.{{name}}.records_a.{{key}}.records
module.{{name}}.records_a.{{key}}.ttl
module.{{name}}.records_a.{{key}}.type
module.{{name}}.records_cname.{{key}}
module.{{name}}.records_cname.{{key}}.name
module.{{name}}.records_cname.{{key}}.id
module.{{name}}.records_cname.{{key}}.records
module.{{name}}.records_cname.{{key}}.ttl
module.{{name}}.records_cname.{{key}}.type
module.{{name}}.record_mx_root.name
module.{{name}}.record_mx_root.id
module.{{name}}.record_mx_root.records
module.{{name}}.record_mx_root.ttl
module.{{name}}.record_mx_root.type
module.{{name}}.record_mx_subdomain.{{key}}
module.{{name}}.record_mx_subdomain.{{key}}.name
module.{{name}}.record_mx_subdomain.{{key}}.id
module.{{name}}.record_mx_subdomain.{{key}}.records
module.{{name}}.record_mx_subdomain.{{key}}.ttl
module.{{name}}.record_mx_subdomain.{{key}}.type
module.{{name}}.records_txt.{{key}}
module.{{name}}.records_txt.{{key}}.name
module.{{name}}.records_txt.{{key}}.id
module.{{name}}.records_txt.{{key}}.records
module.{{name}}.records_txt.{{key}}.ttl
module.{{name}}.records_txt.{{key}}.type
module.{{name}}.subzone.{{key}}
module.{{name}}.subzone.{{key}}.name
module.{{name}}.subzone.{{key}}.id
module.{{name}}.subzone.{{key}}.records
module.{{name}}.subzone.{{key}}.ttl
module.{{name}}.subzone.{{key}}.type
```

## Authors and Maintainers

* Jeff Martin / @jeffersonmartin / jmartin@gitlab.com
* Dillon Wheeler / @dillonwheeler / dwheeler@gitlab.com
